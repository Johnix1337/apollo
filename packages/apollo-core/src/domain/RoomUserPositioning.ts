import {RoomPosition} from "./RoomPosition";

export interface RoomUserPositioning {
    position: RoomPosition,
    direction: number,
}