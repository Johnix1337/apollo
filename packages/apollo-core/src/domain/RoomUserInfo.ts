import {RoomUserPositioning} from "./RoomUserPositioning";
import {UserInfo} from "./UserInfo";
import {RoomUserBase} from "./RoomUserBase";

export interface RoomUserInfo extends RoomUserPositioning {
    id: string;
    user_info: UserInfo;
}