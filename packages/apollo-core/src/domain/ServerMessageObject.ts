export interface ServerMessageObject {
    pid: string;
    pdata: any;
}