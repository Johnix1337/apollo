export interface ClientMessageObject {
    pid: string;
    pdata: any;
}