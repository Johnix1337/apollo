import {RoomUserBase} from "./RoomUserBase";
import {RoomUserPositioning} from "./RoomUserPositioning";

export interface RoomUserPositionChange extends RoomUserBase, RoomUserPositioning {

}