import { RoomUserPositioning } from "./RoomUserPositioning";
import { UserInfo } from "./UserInfo";
export interface RoomUserInfo extends RoomUserPositioning {
    id: string;
    user_info: UserInfo;
}
