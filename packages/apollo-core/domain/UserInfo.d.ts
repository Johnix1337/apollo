export interface UserInfo {
    name: string;
    motto: string;
    id: string;
}
