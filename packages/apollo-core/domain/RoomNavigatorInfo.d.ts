export interface RoomNavigatorInfo {
    id: string;
    name: string;
    description: string;
    user_count: number;
}
