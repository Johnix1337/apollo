import { RoomInfo } from "./RoomInfo";
import { RoomUserInfo } from "./RoomUserInfo";
export interface RoomDetails {
    room_info: RoomInfo;
    user_infos: RoomUserInfo[];
}
