import { RoomNavigatorInfo } from "./RoomNavigatorInfo";
export interface NavigatorInfo {
    rooms: RoomNavigatorInfo[];
}
