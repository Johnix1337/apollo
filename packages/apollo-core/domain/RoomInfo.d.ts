export interface RoomInfo {
    id: string;
    name: string;
    description: string;
    heightmap: number[][];
}
