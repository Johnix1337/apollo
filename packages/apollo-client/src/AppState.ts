import {RoomPosition} from "./RoomPosition";
import {RoomNavigatorInfo, RoomInfo} from "apollo-core/domain";

export interface AvatarState {
    id: string;
    position: RoomPosition;
    previousPosition: RoomPosition;
    positionServerClientDiff: number;

    direction: number;
}

export interface RoomState {
    info: RoomInfo;
    avatars: Map<string, AvatarState>;
}

export interface RoomNavigatorState {
    rooms: RoomNavigatorInfo[];
    window_open: boolean;
}

export interface WorldState {
    room: RoomState | undefined;
    room_navigator: RoomNavigatorState;
    room_creator: RoomCreatorState;
}

export interface PluginCollectionState {
    [id: string]: any;
}

export interface AppState {
    world: WorldState;
    plugins: Map<string, any>;
}

export interface RoomCreatorState {
    window_open: boolean;
}