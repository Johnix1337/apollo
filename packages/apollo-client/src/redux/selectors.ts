import {AppState} from "../AppState";

export function isRoomNavigatorWindowOpen(state: AppState) {
    return state.world.room_navigator.window_open;
}