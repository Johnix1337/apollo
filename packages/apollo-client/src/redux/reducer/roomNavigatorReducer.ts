import {Action} from "../../Action";
import {AppState} from "../../AppState";
import {getType} from "typesafe-actions";
import {serverFetchNavigatorRoomsSuccess, setRoomNavigatorWindowOpen} from "../../actions";

export function roomNavigatorReducer(state: AppState, action: Action): AppState {
    if (action.type == getType(serverFetchNavigatorRoomsSuccess)) {
        return {
            ...state,
            world: {
                ...state.world,
                room_navigator: {
                    ...state.world.room_navigator,
                    rooms: action.payload
                }
            }
        }
    }

    if (action.type === getType(setRoomNavigatorWindowOpen)) {
        return {
            ...state,
            world: {
                ...state.world,
                room_navigator: {
                    ...state.world.room_navigator,
                    window_open: action.payload
                }
            }
        }
    }

    return state;
}