import {AvatarState, AppState} from "../../AppState";
import {Action} from "../../Action";
import {RoomPosition} from "../../RoomPosition";
import {getType} from "typesafe-actions";
import {serverRoomDetails, serverUserJoinedRoom, serverUpdatedRoomPosition} from "../../actions";
import {RoomUserInfo} from "apollo-core/domain";

function avatarStateFromPayload(payload: RoomUserInfo) {
    return {
        id: payload.id,
        position: {
            x: payload.position.x,
            y: payload.position.y
        },
        previousPosition: {
            x: payload.position.x,
            y: payload.position.y
        },
        direction: payload.direction,
        positionServerClientDiff: 0,
    };
}

export function roomReducer(state: AppState, action: Action): AppState {
    if (action.type === getType(serverRoomDetails)) {
        const aMap = new Map<string, AvatarState>();
        const payload = action.payload.user_infos;
        payload.forEach((info) => aMap.set(info.id,  avatarStateFromPayload(info)));

        return {
            ...state,
            world: {
                ...state.world,
                room: {
                    avatars: aMap,
                    info: action.payload.room_info
                }
            }

        };
    }

    if (action.type === getType(serverUserJoinedRoom) && state.world.room) {
        const cp = new Map<string, AvatarState>(<any><unknown>state.world.room.avatars);
        cp.set(action.payload.id, avatarStateFromPayload(action.payload));

        return state = {
            ...state,
            world: {
                ...state.world,
                room: {
                    ...state.world.room,
                    avatars: cp
                },

            },
        };
    }

    if (action.type === getType(serverUpdatedRoomPosition) && state.world.room) {

        const c = new Map<string, AvatarState>(state.world.room.avatars);
        const previous = c.get(action.payload.id);

        if (previous) {
            c.set(action.payload.id, {
                ...previous,
                position: {
                    x: action.payload.position.x,
                    y: action.payload.position.y
                },
                previousPosition: previous.position,
                direction: action.payload.direction,
            });
        }

        return state = {
            ...state,
            world: {
                ...state.world,
                room: {
                    ...state.world.room,
                    avatars: c,
                }
            }

        };
    }

    return state;
}