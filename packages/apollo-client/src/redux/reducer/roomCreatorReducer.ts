import {AppState} from "../../AppState";
import {getType} from "typesafe-actions";
import {setRoomCreatorWindowOpen} from "../../actions";
import {Action} from "../../Action";

export function roomCreatorReducer(state: AppState, action: Action): AppState {
    if (action.type === getType(setRoomCreatorWindowOpen)) {
        return {
            ...state,
            world: {
                ...state.world,
                room_creator: {
                    ...state.world.room_creator,
                    window_open: action.payload
                }
            }
        }
    }

    return state;
}