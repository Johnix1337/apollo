import {Epic} from "redux-observable";
import {Action} from "../../Action";
import {AppState, WorldState} from "../../AppState";

export interface CustomReducerAccessor<T> {
    getCustomState(state: AppState): T;
}

export interface IGameStore {
    createCustomReducer<T>(reducer: (world: WorldState, state: T, action: Action) => T): CustomReducerAccessor<T>;
    addReduxEpic(epic: Epic<Action, Action, AppState>): void;
    dispatch<T>(action: Action | T): void;
}