import {IGameClientDependencies} from "./IGameClientDependencies";
import {IPlugin} from "../IPlugin";

export interface IGamePlugin extends IPlugin<IGameClientDependencies> {
    pluginId: string;
    pluginName: string;
    pluginVersion: string;

    onSetup(dependencies: IGameClientDependencies): void;
}