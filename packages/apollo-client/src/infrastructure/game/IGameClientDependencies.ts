import {IServer} from "../IServer";
import {IGameContext} from "./IGameContext";
import {IGameStore} from "./IGameStore";

export interface IGameClientDependencies {
    server: IServer;
    context: IGameContext;
    store: IGameStore;
}
 