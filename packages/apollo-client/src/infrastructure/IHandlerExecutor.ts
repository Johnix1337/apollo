export interface IHandlerExecutor {
    findAndExecute(type: string, payload: any): void;
}