export interface IServerPacketHandler {
    execute(payload: any): void;
}