export interface IPlugin<TDependencies> {
    onSetup(dependencies: TDependencies): void;
}