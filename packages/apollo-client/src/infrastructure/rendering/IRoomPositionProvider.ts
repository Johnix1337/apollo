export interface IRoomPositionProvider {
    getIsometricPosition(x: number, y: number): { x: number, y: number };
}