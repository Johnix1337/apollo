export interface IServer {
    sendMessage(type: string, payload: any): void;
}