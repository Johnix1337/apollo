import {ClientMessageObject} from "apollo-core/domain";

export namespace ClientMessages {
    function createClientMessage(pid: string, pdata?: any): ClientMessageObject {
        return { pid: pid, pdata: pdata };
    }

    export function fetchNavigatorRooms() {
        return createClientMessage("FETCH_NAVIGATOR_ROOMS");
    }

    export function joinRoom(id: string) {
        return createClientMessage("JOIN_ROOM", { id: id })
    }
}