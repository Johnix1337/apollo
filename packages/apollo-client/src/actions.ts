import {createAction, createStandardAction} from "typesafe-actions";
import { RoomUserInfo, RoomDetails, RoomUserPositionChange, RoomNavigatorInfo, ClientMessageObject } from 'apollo-core/domain';
import { ServerMessageObject } from 'apollo-core/domain';

export const openWebSocket = createAction("WS_OPEN");

export const sendClientMessage = createAction("WS_SEND_PACKET_ACTION", resolve => {
   return (clientMessage: ClientMessageObject) => resolve(clientMessage);
});

export const serverAuthenticateSuccess = createAction("SERVER_AUTHENTICATE_SUCCESS");
export const serverRoomDetails = createAction("SERVER_ROOM_DETAILS", resolve => {
    return (roomDetails: RoomDetails)  => resolve(roomDetails)
});

export const serverUserJoinedRoom = createAction("SERVER_ROOM_USER_INFO_UPDATE", resolve => {
    return (roomUserInfo: RoomUserInfo) => resolve(roomUserInfo);
});

export const serverUpdatedRoomPosition = createAction("SERVER_UPDATED_ROOM_POSITION", resolve => {
    return (positionUpdate: RoomUserPositionChange) => resolve(positionUpdate);
});

export const serverFetchNavigatorRoomsSuccess = createAction("SERVER_FETCH_NAVIGATOR_ROOMS_SUCCESS", resolve => {
    return (roomNavigatorInfos: RoomNavigatorInfo[]) => resolve(roomNavigatorInfos);
});

export const setRoomNavigatorWindowOpen = createAction("SET_ROOM_NAVIGATOR_WINDOW_OPEN", resolve => {
    return (open: boolean) => resolve(open);
});

export const setRoomCreatorWindowOpen = createAction("SET_ROOM_CREATOR_WINDOW_OPEN", resolve => {
    return (open: boolean) => resolve(open);
});