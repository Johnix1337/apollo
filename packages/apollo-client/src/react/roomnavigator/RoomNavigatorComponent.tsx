import {DefaultWindow} from "../window/DefaultWindow";
import * as React from "react";
import {RoomNavigatorInfo} from "apollo-core/domain";
import {DragHandleComponent} from "../window/WindowsComponent";

export function RoomNavigatorComponent(props: {
    DragHandle: DragHandleComponent,
    rooms: RoomNavigatorInfo[],
    onRoomClicked: (room: RoomNavigatorInfo) => void,
    onClose: () => void,
    onCreateRoom: () => void
}) {


    return <DefaultWindow DragHandle={props.DragHandle} title={"Raum Navigator"} onClose={props.onClose}>
        <div className="core-navigator-content">
            <div className="room-list">
                <ul>
                    {props.rooms.map((roomInfo, i) => {
                        const onRoomClicked = () => {
                            props.onRoomClicked(roomInfo);
                            props.onClose();
                        };

                        return (<li key={i} onClick={onRoomClicked}>
                            <div className="user-count">{roomInfo.user_count}</div>
                            <div className="name">{roomInfo.name}</div>
                        </li>);
                    })}

                </ul>
            </div>

            <div className="navigator-button-bar">
                <div className="create-room-button" onClick={props.onCreateRoom}>
                    Raum erstellen
                </div>
            </div>


        </div>
    </DefaultWindow>;
}