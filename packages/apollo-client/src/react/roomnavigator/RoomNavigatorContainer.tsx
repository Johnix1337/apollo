import * as React from "react";
import {connect, DispatchProp} from "react-redux";
import {AppState} from "../../AppState";
import {RoomNavigatorComponent} from "./RoomNavigatorComponent";
import {RoomNavigatorInfo} from "apollo-core/domain";
import {sendClientMessage, setRoomCreatorWindowOpen, setRoomNavigatorWindowOpen} from "../../actions";
import {ClientMessages} from "../../message/ClientMessages";
import {DragHandleComponent} from "../window/WindowsComponent";

export interface RoomNavigatorContainerOwnProps {
    DragHandle: DragHandleComponent
}

export interface RoomNavigatorContainerStateProps {
    rooms: RoomNavigatorInfo[];
}

class RoomNavigatorContainer extends React.Component<RoomNavigatorContainerOwnProps & RoomNavigatorContainerStateProps & DispatchProp> {
    onRoomClicked = (room: RoomNavigatorInfo) => {
        this.props.dispatch(sendClientMessage(ClientMessages.joinRoom(room.id)))
    };

    onClose = () => {
        this.props.dispatch(setRoomNavigatorWindowOpen(false));
    };

    onCreateRoom = () => {
        this.props.dispatch(setRoomCreatorWindowOpen(true));
    };

    componentDidMount(): void {
        this.props.dispatch(sendClientMessage(ClientMessages.fetchNavigatorRooms()));
    }

    render(): React.ReactNode {
        return <RoomNavigatorComponent
            DragHandle={this.props.DragHandle}
            rooms={this.props.rooms}
            onRoomClicked={this.onRoomClicked}
            onClose={this.onClose}
            onCreateRoom={this.onCreateRoom}
        />;
    }
}

const mapStateToProps = (state: AppState): RoomNavigatorContainerStateProps => ({
    rooms: state.world.room_navigator.rooms
});

export default connect(mapStateToProps)(RoomNavigatorContainer);
