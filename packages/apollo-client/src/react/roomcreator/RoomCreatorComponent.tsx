import {DragHandleComponent} from "../window/WindowsComponent";
import {DefaultWindow} from "../window/DefaultWindow";
import * as React from 'react';

export interface RoomCreatorComponentProps {
    DragHandle: DragHandleComponent;
    onClose: () => void;
}

export function RoomCreatorComponent({ DragHandle, onClose }: RoomCreatorComponentProps) {
    return (<DefaultWindow DragHandle={DragHandle} title="Raum erstellen" onClose={onClose}>
        <div>Test</div>
    </DefaultWindow>);
}