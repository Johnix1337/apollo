import {connect, DispatchProp} from "react-redux";
import * as React from "react";
import {AppState} from "../../AppState";
import {RoomCreatorComponent} from "./RoomCreatorComponent";
import {DragHandleComponent} from "../window/WindowsComponent";
import {setRoomCreatorWindowOpen} from "../../actions";

export interface RoomCreatorContainerOwnProps {
    DragHandle: DragHandleComponent
}

class RoomCreatorContainer extends React.Component<RoomCreatorContainerOwnProps & DispatchProp> {
    onClose = () => {
        this.props.dispatch(setRoomCreatorWindowOpen(false));
    };

    render(): React.ReactNode {
        return <RoomCreatorComponent DragHandle={this.props.DragHandle} onClose={this.onClose} />;
    }
}

const mapStateToProps = (state: AppState) => ({

});

export default connect(mapStateToProps)(RoomCreatorContainer);