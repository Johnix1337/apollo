import * as React from 'react';
import {Store} from "redux";
import {Provider} from "react-redux";
import {AppState} from "../AppState";
import {Action} from "../Action";
import {GameCanvasComponent} from "./GameCanvasComponent";
import NavigationBar from "./NavigationBarContainer";
import {WindowsComponent} from "./window/WindowsComponent";
import WindowsContainer from "./window/WindowsContainer";

interface GameAppProps {
    store: Store<AppState, Action>;
    onReady: () => void;
}
interface NavigationElement { element: () => React.ReactNode, priority: number; }

interface GameAppState {
}


export class GameAppComponent extends React.Component<GameAppProps, GameAppState> {
    constructor(props: GameAppProps) {
        super(props);
    }

    componentDidMount(): void {
    }

    render(): React.ReactNode {
        return (<Provider store={this.props.store}>
            <div id="game-container">
                <GameCanvasComponent onReady={this.props.onReady} />
                <NavigationBar />
                <WindowsContainer/>
            </div>
        </Provider>);
    }

    /*
    addNavigationIcon(element: () => React.ReactNode, priority: number): void {
        console.log("Add element");

        this.setState((prevState) => {
            if (prevState.navigationElements.findIndex((e) => e.element == element) != -1) return prevState;

            const newArr = [...prevState.navigationElements, { element: element, priority: priority }];
            newArr.sort((a,b) => a.priority - b.priority);

            return {
                ...prevState,
                navigationElements: newArr
            };
        });
    }

    removeNavigationIcon(element: () => React.ReactNode): void {
        this.setState((prevState) => {
            const index = prevState.navigationElements.findIndex((e) => e.element == element);
            if (index != -1) {
                const newArr = [...prevState.navigationElements].splice(index, 1);

                return {
                    ...prevState,
                    navigationElements: newArr
                }
            }


            return prevState;
        });
    }

    createWindow(render: WindowRender): void {
        this.setState((prevState) => {
            if (prevState.windows.findIndex((e) => e == render) != -1) return prevState;

            const newArr = [...prevState.windows, render];

            return {
                ...prevState,
                windows: newArr
            };
        });
    }

    deleteWindow(render: WindowRender): void {
    }*/
}