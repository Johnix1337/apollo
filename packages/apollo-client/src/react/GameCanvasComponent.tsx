import * as React from "react";

export interface GameCanvasProps {
    onReady: () => void;
}

export class GameCanvasComponent extends React.Component<GameCanvasProps> {
    componentDidMount(): void {
        this.props.onReady();
    }

    render(): React.ReactNode {
        return (<div id="canvas" />);
    }
}