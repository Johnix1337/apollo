import * as React from "react";

export interface IconNavigationElementProps {
    imageUrl: string;
    onClick: () => void;
}

export class IconNavigationComponent extends React.Component<IconNavigationElementProps> {
    constructor(props: IconNavigationElementProps) {
        super(props);
    }

    render(): React.ReactNode {
        return <div onClick={this.props.onClick} className="navigation-icon" style={{ backgroundImage: `url(${this.props.imageUrl})` }} />;
    }
}