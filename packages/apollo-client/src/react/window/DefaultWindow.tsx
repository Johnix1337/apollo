import * as React from "react";
import {Attributes, ReactNode} from "react";
import {DragHandleComponent} from "./WindowsComponent";

export function DefaultWindow({DragHandle, title, children, onClose}: { DragHandle: DragHandleComponent, title: string, children: ReactNode, onClose: () => void } & Attributes) {
    return (<div className="window">
        <div className="window-bar">
            {title}
            <DragHandle />
            <div className="window-bar-close" onClick={onClose} />
        </div>
        <div className="window-content">
            {children}
        </div>
    </div>);
}