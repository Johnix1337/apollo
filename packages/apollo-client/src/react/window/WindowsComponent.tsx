import * as React from "react";
import {useState} from "react";
import {useEffect} from "react";
import {ReactNode} from "react";
import {connect} from "react-redux";
import {AppState} from "../../AppState";
import {RoomNavigatorComponent} from "../roomnavigator/RoomNavigatorComponent";
import RoomNavigatorContainer from "../roomnavigator/RoomNavigatorContainer";
import {ReactElement} from "react";
import RoomCreatorContainer from "../roomcreator/RoomCreatorContainer";

export interface DragHandleProps { children?: ReactNode }

export interface DragHandleComponent {
    (props: DragHandleProps): ReactElement<DragHandleProps>
}

export interface WindowRenderProps {
    DragHandle: DragHandleComponent
}


export type WindowRender = (props: WindowRenderProps) => React.ReactElement<WindowRenderProps>;

/**
 * Wraps around a WindowRender. Injects a DragHandle into the WindowRender to provide dragging functionality for the window.
 * @param props
 * @constructor
 */
function WindowWrapper(props: { windowRender: WindowRender }) {
    const [position, setPosition] = useState({ x: 50, y: 50 });
    const [dragging, setDragging] = useState<{x:number;y:number}|null>(null);

    const mouseUp = (e: any) => {
        setDragging(null);
    };

    const createMouseMove = (dragging: {x:number;y:number}|null) => (e: any) => {
        if (!dragging) return;

        setPosition({x: e.clientX - dragging.x, y: e.clientY- dragging.y});
    };

    useEffect(() => {

        const mouseMove = createMouseMove(dragging);

        if (dragging) {
            console.log("DRAGGING");
            document.addEventListener("mousemove", mouseMove);
            document.addEventListener("mouseup", mouseUp);
        }

        return function cleanup() {
            document.removeEventListener("mousemove", mouseMove);
            document.removeEventListener("mouseup", mouseUp);
        };
    }, [dragging]);

    const onMouseDown = (e: any) => {
        setDragging({ x: e.clientX - position.x, y: e.clientY - position.y });
    };


    // DragHandle is a helper calls which can be used to wrap around elements to act as a drag handle.
    // Usually in a window, you only want the top bar to be the drag handle
    const DragHandle: DragHandleComponent = (props) => {
        return <div style={{ height: '100%', width: '100%', position: 'absolute', top: 0, left: 0 }} onMouseDown={onMouseDown} onMouseUp={mouseUp} onDragEnd={mouseUp} onDrag={createMouseMove(dragging)}>{props.children}</div>
    };

    return (<div className="window-wrapper" style={{ top: position.y, left: position.x }}>
        {props.windowRender({ DragHandle: DragHandle })}
    </div>);
}

export interface WindowManagerOwnProps {
    windows: WindowInfo[];
}

export interface WindowInfo {
    id: string;
    windowRender: WindowRender;
}

export class WindowsComponent extends React.Component<WindowManagerOwnProps> {
    constructor(props: WindowManagerOwnProps) {
        super(props);
    }

    render(): React.ReactNode {
        return (<div>
            {this.props.windows.map((windowInfo) => <WindowWrapper key={windowInfo.id} windowRender={windowInfo.windowRender} />)}
        </div>);
    }
}