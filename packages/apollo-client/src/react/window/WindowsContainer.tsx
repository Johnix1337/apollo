import * as React from "react";
import {connect} from "react-redux";
import {AppState} from "../../AppState";
import {WindowInfo, WindowsComponent} from "./WindowsComponent";
import RoomCreatorContainer from "../roomcreator/RoomCreatorContainer";
import RoomNavigatorContainer from "../roomnavigator/RoomNavigatorContainer";

interface WindowsContainerStateProps {
    roomNavigatorOpen: boolean;
    roomCreatorOpen: boolean;
}

const RoomCreatorWindowInfo: WindowInfo = {
    id: "RoomCreator",
    windowRender: (props) => <RoomCreatorContainer {...props} />
};

const RoomNavigatorWindowInfo: WindowInfo = {
    id: "RoomNavigator",
    windowRender: (props) => <RoomNavigatorContainer {...props} />
};

class WindowsContainer extends React.Component<WindowsContainerStateProps> {
    render() {
        const windowInfos: WindowInfo[] = [];

        if (this.props.roomNavigatorOpen) windowInfos.push(RoomNavigatorWindowInfo);
        if (this.props.roomCreatorOpen) windowInfos.push(RoomCreatorWindowInfo);

        return (<WindowsComponent windows={windowInfos} />)
    }
}

const mapStateToProps = (state: AppState): WindowsContainerStateProps => ({
    roomCreatorOpen: state.world.room_creator.window_open,
    roomNavigatorOpen: state.world.room_navigator.window_open
});

export default connect(mapStateToProps)(WindowsContainer);