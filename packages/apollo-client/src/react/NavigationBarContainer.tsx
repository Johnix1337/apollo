import * as React from "react";
import {IconNavigationComponent} from "./navigation/IconNavigationComponent";
import {connect, DispatchProp} from "react-redux";
import {AppState} from "../AppState";
import {setRoomNavigatorWindowOpen} from "../actions";
import {isRoomNavigatorWindowOpen} from "../redux/selectors";

export interface NavigationBarOwnProps {
    showLeaveRoomIcon: boolean;
    roomNavigatorWindowOpen: boolean;
}

class NavigationBarContainer extends React.Component<NavigationBarOwnProps & DispatchProp> {
    onNavigatorClick = () => {
        this.props.dispatch(setRoomNavigatorWindowOpen(!this.props.roomNavigatorWindowOpen));
    };

    onHotelClick = () => {
    };

    render(): React.ReactNode {

        const navigationElements: React.ReactNode[] = [];

        if (this.props.showLeaveRoomIcon) {
            navigationElements.push(<IconNavigationComponent imageUrl={"images/hotel.png"} onClick={this.onHotelClick} />);
        }

        navigationElements.push(<IconNavigationComponent imageUrl={"images/navigator.png"} onClick={this.onNavigatorClick} />);


        return (<div id="navigation-bar">
            <ul id="navigation-elements">
                {navigationElements.map((element, i) => <li key={i}>{element}</li>)}
            </ul>
        </div>);
    }
}

const mapStateToProps = (state: AppState): NavigationBarOwnProps => ({
    showLeaveRoomIcon: !!state.world.room,
    roomNavigatorWindowOpen: isRoomNavigatorWindowOpen(state)
});


export default connect(mapStateToProps)(NavigationBarContainer);