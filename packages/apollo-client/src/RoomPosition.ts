export interface RoomPosition {
    x: number;
    y: number;
}

export function positionEquals(pos1: RoomPosition, pos2: RoomPosition) {
    return pos1.x == pos2.x && pos1.y == pos2.y;
}