import {Action} from "./Action";
import {IServer} from "./infrastructure/IServer";
import {filter, flatMap, ignoreElements, map, tap} from "rxjs/operators";
import {EMPTY, merge, of} from "rxjs";
import {isActionOf} from "typesafe-actions";
import {
    serverAuthenticateSuccess,
    openWebSocket,
    sendClientMessage,
    serverRoomDetails, serverUserJoinedRoom, serverUpdatedRoomPosition, serverFetchNavigatorRoomsSuccess
} from "./actions";
import {IGameStore} from "./infrastructure/game/IGameStore";
import {ServerMessageObject} from "apollo-core/domain";

const actionMap = new Map<string, (payload: any) => Action>();
actionMap.set("AUTHENTICATION_SUCCESS", () => serverAuthenticateSuccess());
actionMap.set("ROOM_DETAILS", (payload) => serverRoomDetails(payload));
actionMap.set("USER_JOINED_ROOM", (payload) => serverUserJoinedRoom(payload));
actionMap.set("UPDATED_ROOM_POSITION", (payload) => serverUpdatedRoomPosition(payload));
actionMap.set("FETCH_NAVIGATOR_ROOMS_SUCCESS", (payload) => serverFetchNavigatorRoomsSuccess(payload));

export class WebSocketConnection implements IServer {
    private ws?: WebSocket;
    private authenticated: boolean = false;

    constructor(private store: IGameStore) {
        this.store.addReduxEpic((action$, state, deps) => {
            return merge(
                action$.pipe(
                    filter(isActionOf(openWebSocket)),
                    map((action) => this.sendMessageAction("AUTHENTICATE", { id: prompt("User Id") })),
                ),
                action$.pipe(
                    filter(isActionOf(sendClientMessage)),
                    tap((action) => this.ws && this.ws.send(JSON.stringify(action.payload))),
                    ignoreElements()
                )
            )
        });
    }

    sendMessageAction(type: string, payload: any): Action {
        return sendClientMessage({ pid: type, pdata: payload });
    }

    sendMessage(type: string, payload: any) {
        this.store.dispatch(this.sendMessageAction(type, payload));
    }

    listen() {
        const context = this.store;
        this.ws = new WebSocket("ws://localhost:8080");

        this.ws.onopen = () => {
            context.dispatch({ type: "WS_OPEN" });
        };

        this.ws.onmessage = (message) => {
            const packet: ServerMessageObject = JSON.parse(message.data);
            const action = actionMap.get(packet.pid);

            console.log("RECEIVED MESSAGE", packet.pid, "Has action:", !!action);

            if (action) {
                context.dispatch(action(packet.pdata));
            }


        };
    }
}