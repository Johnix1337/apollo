export interface LoopState {
    delta: number;
    resized: boolean;
}