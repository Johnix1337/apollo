export function drain<T>(arr: T[], cb: (elem: T) => void) {
    let p: T | undefined;

    do {
        p = arr.shift();
        if (!p) return;

        cb(p);
    } while (p);
}