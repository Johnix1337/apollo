import {IGameContext} from "./infrastructure/game/IGameContext";
import {Action} from "./Action";
import {drain} from "./Util";
import * as PIXI from "pixi.js";
import {AppState, WorldState} from "./AppState";
import {Renderer} from "./rendering/Renderer";
import {WebSocketConnection} from "./WebSocketConnection";
import {roomReducer} from "./redux/reducer/roomReducer";
import {IGamePlugin} from "./infrastructure/game/IGamePlugin";
import {IGameClientDependencies} from "./infrastructure/game/IGameClientDependencies";

import {applyMiddleware, createStore, Store} from 'redux';
import {combineEpics, createEpicMiddleware, Epic,} from "redux-observable";
import {CustomReducerAccessor, IGameStore} from "./infrastructure/game/IGameStore";
import {BehaviorSubject, combineLatest, defer, Observable, Subject} from "rxjs";
import * as React from "react";
import {GameAppComponent} from "./react/GameAppComponent";
import * as ReactDOM from "react-dom";
import {roomNavigatorReducer} from "./redux/reducer/roomNavigatorReducer";
import {roomCreatorReducer} from "./redux/reducer/roomCreatorReducer";

function notEmpty<T>(e: T|null|undefined): e is T {
    return !!e;
}

export class Game implements IGameContext, IGameStore {

    private app: PIXI.Application;

    private middlewares: Epic<Action, Action, AppState>[] = [];
    private pluginReducers: { id: string; reducer: (state: WorldState, pluginState: any, action: Action) => any }[] = [];

    private plugins: IGamePlugin[] = [];
    private store?: Store<AppState, Action>;
    private stateSubject = new Subject<AppState>();
    private renders: (() => React.ReactNode)[] = [];


    constructor() {
        this.app = new PIXI.Application(window.innerWidth, window.innerHeight, {
            backgroundColor: 0x000000
        });
    }

    getStore(): Store<AppState, Action> {
        if (!this.store) throw new Error("Store not created yet");
        return this.store;
    }

    addPlugin(plugin: IGamePlugin) {
        this.plugins.push(plugin);
    }


    addReduxEpic(epic: Epic<Action, Action, AppState>): void {
        if (this.store) throw new Error("Can't add epics after store created");

        this.middlewares.push(epic);
    }


    private index = 0;
    createCustomReducer<T>(reducer: (state: WorldState, pluginState: T, action: Action) => T): CustomReducerAccessor<T> {

        const id = (this.index++).toString();

        this.pluginReducers.push({ id: id, reducer: reducer });

        return {
            getCustomState: (state: AppState) => {
                return state.plugins.get(id) as T;
            }
        }
    }

    dispatch(action: Action): void {
        if (!this.store) throw new Error("Can't dispatch when store is not setup");
        this.store.dispatch(action);
    }

    private setupPlugins(dependencies: IGameClientDependencies) {
        this.plugins.forEach((plugin) => plugin.onSetup(dependencies));
    }


    private createMainReducer() {
        const _this = this;
        function pluginReducer(state: AppState, action: Action): AppState {
            const map = new Map();
            _this.pluginReducers.forEach((pluginReducer) => {
                map.set(pluginReducer.id, pluginReducer.reducer(state.world, state.plugins.get(pluginReducer.id), action))
            });

            return {
                ...state,
                plugins: map
            }
        }

        const reducers = [
            roomReducer,
            roomNavigatorReducer,
            pluginReducer,
            roomCreatorReducer
        ];

        const initialState: AppState = {
            plugins: new Map(),
            world: {
                room: undefined,
                room_navigator: {
                    rooms: [],
                    window_open: false
                },
                room_creator: {
                    window_open: false
                }
            },
        };

        return (state = initialState, action: Action) => {
            return reducers.reduce((state, reducer) => {
                return reducer(state, action);
            }, state);
        };
    }

    private createGameStore(): Store<AppState, Action> {
        const mainReducer = this.createMainReducer();

        const mw = createEpicMiddleware<Action, Action, AppState>();
        const store = createStore(mainReducer, applyMiddleware(mw));
        mw.run(combineEpics(...this.middlewares));

        return store;
    }

    run() {
        const ws = new WebSocketConnection(this);
        const renderer = new Renderer(this.app, (pos) => {
            ws.sendMessage("ROOM_MOVE_USER", { x: pos.x, y: pos.y });
        });

        const dependencies: IGameClientDependencies = {
            server: ws,
            context: this,
            store: this,
        };

        this.setupPlugins(dependencies);

        this.store = this.createGameStore();
        this.stateSubject.next(this.store.getState());
        const store = this.store;


        store.subscribe(() => this.stateSubject.next(store.getState()));

        renderer.startRenderLoop(this.store);
        ws.listen();

        const element = React.createElement(GameAppComponent, {
            store: this.store,
            onReady: () => renderer.startRenderLoop(store)
        });
        ReactDOM.render(element, document.getElementById("dom-root"));
    }

    addComponent(render: () => React.ReactNode): void {
        this.renders.push(render);
    }
}