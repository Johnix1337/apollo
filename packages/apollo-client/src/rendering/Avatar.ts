import {IRenderer} from "../infrastructure/IRenderer";
import {positionEquals, RoomPosition} from "../RoomPosition";
import * as PIXI from "pixi.js";
import {IRoomPositionProvider} from "../infrastructure/rendering/IRoomPositionProvider";
import {AvatarState} from "../AppState";
import {IAvatarContainer} from "./Room";
import DisplayObject = PIXI.DisplayObject;

export class Avatar {
    private nextPosition: RoomPosition | null = null;
    private readonly sprite: PIXI.Sprite;

    private animStarted: number | null = null;
    private animDuration: number = 0;

    private direction: number;

    private nextPositionWait: { position: RoomPosition, direction: number, duration: number }[] = [];
    private currentPosition: RoomPosition;
    private avatarContainer: PIXI.Container | null = null;


    private static getAvatarDirection(direction: number): PIXI.Texture {
        return PIXI.Texture.from(`http://localhost:9001/image?figure=hd-180-1.ch-255-66.lg-280-110.sh-305-62.ha-1012-110.hr-828-61&size=b&direction=${direction}&head_direction=${direction}`);
    }

    constructor(private roomPositionProvider: IRoomPositionProvider, private container: IAvatarContainer, private avatarState: AvatarState) {
        this.sprite = new PIXI.Sprite(Avatar.getAvatarDirection(this.avatarState.direction));
        this.direction = avatarState.direction;
        this.currentPosition = avatarState.position;

    }

    private resetNextPosition() {
        this.nextPosition = null;
    }

    setPosition(position: RoomPosition, direction: number, duration: number) {
        if (positionEquals(position, this.currentPosition) || (this.nextPosition && positionEquals(position, this.nextPosition))) return;


        this.currentPosition = this.nextPosition || this.currentPosition;
        this.nextPosition = position;
        this.direction = direction;
        this.animStarted = Date.now();
        this.animDuration = duration;

    }

    zIndex() {
        return this.currentPosition.x + this.currentPosition.y;
    }

    update(delta: number) {
        const sprite = this.sprite;
        sprite.texture = Avatar.getAvatarDirection(this.direction);

        const startIsometric = this.roomPositionProvider.getIsometricPosition(this.currentPosition.x, this.currentPosition.y);
        let x = startIsometric.x;
        let y = startIsometric.y;



        let diff = (this.animStarted == null || this.animDuration <= 0) ? 1 : (Date.now() - this.animStarted) / this.animDuration;
        if (diff > 1) diff = 1;

        if (this.nextPosition) {
            this.container.updateContainerOrder(this, this.nextPosition);

            const targetIsometric = this.roomPositionProvider.getIsometricPosition(this.nextPosition.x, this.nextPosition.y);
            x = startIsometric.x + (targetIsometric.x - startIsometric.x) * diff;
            y = startIsometric.y + (targetIsometric.y - startIsometric.y) * diff;
            if (diff == 1) {
                this.currentPosition = this.nextPosition;
                this.resetNextPosition();
            }
        }

        sprite.x  = x -1;
        sprite.y = y - 86;
    }

    getSprite(): DisplayObject {
        const sprite = this.sprite as any;
        sprite.roomX = (this.nextPosition && this.nextPosition.x) || this.currentPosition.x;
        sprite.roomY = (this.nextPosition && this.nextPosition.y) || this.currentPosition.y;
        return this.sprite;
    }
}