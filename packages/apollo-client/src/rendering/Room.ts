import {IRenderer} from "../infrastructure/IRenderer";
import * as PIXI from "pixi.js";
import {AvatarState, RoomState} from "../AppState";
import {IRoomPositionProvider} from "../infrastructure/rendering/IRoomPositionProvider";
import DisplayObject = PIXI.DisplayObject;
import {Avatar} from "./Avatar";
import {RoomPosition} from "../RoomPosition";
import Sprite = PIXI.Sprite;


const TILE_HEIGHT = 32;
const TILE_WIDTH = 64;
const imageUrl = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAEAAAABuCAYAAACXzxWYAAAACXBIWXMAAA7EAAAOxAGVKw4bAAAJIklEQVR4nO1cT2gb2R3+3ljVdj3EqMZWsUAuqFBvKWzNbuXDsjTgwx5EDyZQdKjjHJYcXEJ9yV7sw6wOzh7qS8qSUHqS6otaWHwoOqQgkI9Jmo0XlkaUNY0MYxwLVwisrrWyXg/ye34znpFGnvc0MvVnBsma0ej3fb8/769EKCj+n6EFbUDQuBYgaAOCxrUAQRsQNK4FCNqAoHEtQNAGBI1rAYI2IGhcCxC0AUHjWoCgDQga1wIEbUDQuBZA5s0ICBUfrwKkCEBAKAGh09PTEB+vghDE76QoI1yv11Gr1cCej42NAQAqlQooKJFhrAr4igACQiORCCqVCsbGxhCJRHB6esrJNxoNsGiQYq0ChGTcRPT66ekp6vU6AEDXdf7ciwhBRMqlU0AMfRG6rmNkZIS/XqvVMDEx4eme1WqVPx+UGL5qAAGhLNxv3LiB4+Njfq5WqwGAZ/J2MDFUC+ErAiKRCHRdx/HxMdrtNj9Xr9cRiUQQCvXOsNHR0a7nVRdRXwKMjY1B0zS0222Ew2E0m01OnsEuQi/CTlApgrQUAMALoaZdbFzE6y4DVSJIaQUAWNp+O3Rdx+npKUZGRhzPNxoNWWb0DSkdIcDqYRYBuq6D0s79Cek4j1LK06Jf4tVqVXoUSIkAN88z8uw5IQSEENTrdU8FchBQYoWu6/w5Iw7A0ky2Wq2OAX0IMTExAVIlVGYUSBcgHA7j5OQE4XCYexywkhfRarX6ailki+BLAHsrEA6HLecppZ7yvNVqeW4lLtOMdkPgicgI2SOBpYgdoVCoM8CqyIkCaQLYvd9sNl1JMNi9ab/eqT44pYwfKIuAZrPp2CECeocxI1ipVC6cm56e7ilsP1AiQLPZvNT77MTbTx9Dm1tG++ljfo02t4zp6Wn/RrLPlHYnD3DzvBNxoEP2s48/wH9/tgQAePvrnHSbpAgg5r/o/Xa77ZoGgDtxEb/7w995v8LpvF/4mhKjoMQ+IeKE0dHRC94PhUKoVCqoVCoWj2tzy35M6htSp8Wdcl+cJ2Bg5NtPH/M8/+zjD3B8fOzaYRK9L3Nk6FsACkrEqSw72DwBO0TyIlioi93o57lV6LquJPQZpESAmwj2vsHo6KgjeTeC770Td/28S5p6AdJSoFc9cCNvh/3889yq0rogfW2QiSB63wv5t7/OOZ53iwJZkNYPULH4wTz/PLcq+9YcvmeEgHPyk5OT0DQNBwcHfDpc9H759QFmfvRDT/d88WoPwMUI0OaWpdYA3xFAQOj+1gymFso4PDzseu32l/8CAE8iOIW+bPKAxBqwvzVj+b9araJarVoGNHcXPsRPf/2p53u+eLWnvHMkpQZsPqlh8aPIBRFEsEGN0wBHxItXe/jF0gP+P7uu/PpAhqkXIGVW+Pe/7YT04keRrtdOLZQt3V6vHZzy6wNL5AxlPwDoRMLmkxqmFso4aVJMLZT5sb3TwP7WDA9nFgm9PMvIry1NYm1pEoDcFkf6cPiTRwfIZ+J4K0zw77/8BH/6238AAGljD/lMnIsgpgMA/POvn164l71e3Ls1DgBYzx2CQM6UmNQUYOR/+fPzkd9Jk3IR1nOH/LyYDgBcC93a0iTWc4dYW5rEvVvj+K5F8b0QwdRCWUoqKN8l9laY4O6vfgCgQyZt7FnSQUwJ8WDXMzDvM+xvzUhJBWkCOHmfoZsIYl3gRs0tc/J24rLhuwZQUEIeefPEvVvj+PyLIy4Cg1gXBo2Bb5RkHhWr+tRCuWPMWUoMyvuApFaAgpJ+8pFFAoOY673ACmC3Tlc/CGyr7L1b4109PAjvAwOaFt/eaSBt7Ll6bVBknTCQCEgbe1hbmrSEvR/kM3FeN/xCuQDbO8Ftf/GCod8uzwqeLI/bMfQCqMaVEECV94GABfAS3qy7vL81g+2dhnQxrkQEMLBxRrcmtV8ELkC/Hs1n5K4TBLpH6DJedBpt+kHgEQAAn39x5DkSNp/UpH52oBHgp5N0ZXqCbmDkv/rmW6znDsGm1bpB9L6suYNAI+Crb771dJ1I/JNHctcHAokAMfRF79vzm02zMzDybJ1RBgKJAOZ5p9B3KnKMOJt4YWsFQ7VTFPDWQdneaWA911lE7ZX3jHgsFoNpmvx1WeQByQI8z61iaqGzrtdNiH6I2zGUX5hgeO+duGXtD7D23N798fdROXDeRSoWNyfiqiBlgwTQWSFyapq8Lm2LpE3TdBTBNM3h3R/Ab3hG+MtyZ96//fQx/rj6GwBAJBLhRzQaRTQaRSwW80ReFaQKwEgDwPu3H/DX7i58aLkuHA4jFAq5bnuXuRu8F6TVAApKyO3O2oBdCJYaoVAImqYNzRemAMlFkOWnXQhtbhnj4/6nvmOxGIgp90tTSnqCFJRQUPL+7Qf4x5/VbXGTAWmtgOPNz5bLmPc1TeuZAqZpIhqNul4juyVQPhYQv0g9jBiKCZEgoUwA9vsCw46hi4BYLIY3b944nlPRExyeBrkL2EhQxe8HKIkAWeFvmiZM04RhGBKscsbQRgDzerFYxPz8PAzDAMnI7QQBQyaAOOmRSqUAAPPz80pFUCIABSWk1l8a2Mknk0ncvHkTwLkIGxsbyGazIHfkiaCsFaCghP2WUDfY81wkPz8/j3Q6DcMwsLGxgc3NTeTzeRSLRXn7haniPwAUAB0fH6cTExM0Go3SWCxG2esAaLFYpACoYRiWx2w2S4vFIj06OqLFYpGmUil6dHTEXwdAfds3CAGy2ayFMDtSqRRNpVKuIjDy4iG+JkMEpUWQhWk+n0c2m0U83pkfLJVKyGQyuH//Pvb2OkNme57fuXPHkg6zs7MAwB9lQXkrwEiIhDKZDIrFIhcim80imUzyPF9cXOSVn6FUKiGRSPD/d3d38ezZM9/2KR0OA+dRkEqlkE6nLwjBImN2dhYvX75EqVTCysoKHj58yAshey8AxONxHjW7u7vIZDKgfloE1TVArAU4y3tWE/rJc7fDr10D6wgxL5ECoYVCAalUColEAhsbG0gmk1hZWeHXinleKpUs75cN5Sng+sFnqWEYBhKJBPL5PAAgmUwCABKJBM/zQqGgTIDAusI8IjLnQgAd4izPWdErFArK7AgsAuzo1rNT5X1giAQICkM3IzRo/A/mDcM8mlYZ0gAAAABJRU5ErkJggg==";

const textureTile = PIXI.Texture.from("images/tile.png");
const textureTileActive = PIXI.Texture.from("images/tile_active.png");
const textureYWall = PIXI.Texture.from("images/wall.png");
const textureXWall = PIXI.Texture.from("images/wall_x.png");
const textureCornerWall = PIXI.Texture.from("images/wall_corner.png");

export interface IAvatarContainer {
    updateContainerOrder(avatar: Avatar, position: RoomPosition): void;
}

export class Room implements IRoomPositionProvider, IAvatarContainer {
    private appCenterX: number = 0;
    private appCenterY: number = 0;
    private topLeftCornerX: number = 0;

    private width: number = 0;
    private height: number = 0;

    private tileContainer: PIXI.Container;
    private avatarContainer: PIXI.Container;
    private container: PIXI.Container;

    private tileSprites: (PIXI.DisplayObject | null)[][] = [];
    private avatars: Map<string, Avatar> = new Map();


    constructor(private app: PIXI.Application, private room: RoomState, private onClickTile: (pos: RoomPosition) => void) {


        let xMax = this.room.info.heightmap[0].length;
        let yMax = this.room.info.heightmap.length;

        this.tileSprites = [];
        for (let y = 0; y < yMax; y++) {
            const row = [];
            for (let x = 0; x < xMax; x++) {
                row.push(null);
            }
            this.tileSprites.push(row);
        }

        this.container = new PIXI.Container();
        this.tileContainer = new PIXI.Container();

        this.avatarContainer = new PIXI.Container();
        this.container.addChild(this.avatarContainer);


        this.updateTiles();
    }

    getIsometricPosition(x: number, y: number): { x: number, y: number } {
        return {
            x: this.appCenterX - this.width/2 + this.topLeftCornerX + (x - y) * 32,
            y: this.appCenterY - this.height/2 + (x + y) * 16
        };
    }

    getDisplayObject(): DisplayObject {
        return this.container;
    }

    addAvatar(avatarState: AvatarState) {
        let avatar = this.avatars.get(avatarState.id);
        if (!avatar) {
            avatar = new Avatar(this, this, avatarState);
            this.avatars.set(avatarState.id, avatar);
            this.avatarContainer.addChild(avatar.getSprite())
        }

        avatar.setPosition(avatarState.position, avatarState.direction, 500);
    }

    private updateTiles(): PIXI.Container {
        let xMax = this.room.info.heightmap[0].length;
        let yMax = this.room.info.heightmap.length;

        const topLeftCornerX = (yMax-1) * (TILE_WIDTH/2);
        const topLeftCornerY = 0;

        const bottomRightCornerX = (xMax+1) * (TILE_WIDTH / 2);
        const bottomRightCornerY = (yMax + xMax) * (TILE_HEIGHT / 2);

        const height = topLeftCornerY + bottomRightCornerY;
        const width = topLeftCornerX + bottomRightCornerX;

        const container = new PIXI.Container();

        const appCenterX = this.app.screen.width / 2;
        const appCenterY = this.app.screen.height / 2;

        this.height = height;
        this.width = width;

        this.appCenterX = appCenterX;
        this.appCenterY = appCenterY;

        this.topLeftCornerX = topLeftCornerX;

        for (let y = 0; y < yMax; y++) {
            for (let x = 0; x < xMax; x++) {
                let createdTile: PIXI.DisplayObject|null = null;
                const tileValue = this.room.info.heightmap[y][x];
                if (tileValue == 1) {
                    const tile = new PIXI.Sprite(textureTile);


                    tile.anchor.set(0,0);

                    const isometric = this.getIsometricPosition(x,y);

                    tile.x = isometric.x;
                    tile.y = isometric.y;

                    tile.interactive = true;
                    tile.buttonMode = true;


                    tile.on("mouseover", () => {
                        tile.texture = textureTileActive;
                        tile.y = appCenterY - height/2 + (x + y) * 16 - 3;
                        console.log("Change texture");
                    });

                    tile.on("mouseout", () => {
                        tile.texture = textureTile;
                        tile.y = appCenterY - height/2 + (x + y) * 16;
                    });

                    tile.on("pointertap", () => {
                        this.onClickTile({ x: x, y: y });
                    });

                    tile.hitArea = new PIXI.Polygon([
                        new PIXI.Point(0, 16),
                        new PIXI.Point(32, 0),
                        new PIXI.Point(64, 16),
                        new PIXI.Point(32, 32)
                    ]);

                    createdTile = tile;

                    //tileContainer.addChild(tile);
                }

                if (tileValue == 2) {
                    const tile = new PIXI.Sprite(textureYWall);


                    tile.anchor.set(0,0);

                    const isometric = this.getIsometricPosition(x,y);

                    tile.x = isometric.x + 8 + 16;
                    tile.y = isometric.y - 143 + 32 + 8;

                    createdTile = tile;
                }

                if (tileValue == 3) {
                    const tile = new PIXI.Sprite(textureXWall);


                    tile.anchor.set(0,0);

                    const isometric = this.getIsometricPosition(x,y);

                    tile.x = isometric.x - 1;
                    tile.y = isometric.y - 143 + 32 + 8;

                    createdTile = tile;
                }

                if (tileValue == 4) {
                    const tile = new PIXI.Sprite(textureCornerWall);


                    tile.anchor.set(0,0);

                    const isometric = this.getIsometricPosition(x,y);

                    tile.x = isometric.x + 26;
                    tile.y = isometric.y - 90;

                    createdTile = tile;
                }

                this.tileSprites[y][x] = createdTile;
                if (createdTile) container.addChild(createdTile);
            }
        }

        if (this.tileContainer) this.container.removeChild(this.tileContainer);
        this.tileContainer = container;
        this.container.addChild(this.tileContainer);

        this.container.swapChildren(this.tileContainer, this.avatarContainer);

        return container;
    }

    update(delta: number, resized: boolean) {
        if (resized) {
            this.updateTiles();
        }

        this.avatars.forEach((avatar) => {
            avatar.update(delta);
        });
    }

    updateContainerOrder(avatar: Avatar, position: RoomPosition): void {
        const sprite: any = avatar.getSprite();
        sprite.zIndex = position.x + position.y;

        console.log("Update order Z-Index", sprite.zIndex);

        this.avatarContainer.children.sort((a: any,b: any) => {
            console.log(a,b);

            a.zIndex = a.zIndex || 0;
            b.zIndex = b.zIndex || 0;
            return a.zIndex - b.zIndex
        });

    }
}