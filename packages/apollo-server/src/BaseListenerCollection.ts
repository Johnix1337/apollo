export abstract class BaseListenerCollection<T> {
    private listeners: T[] = [];

    add(listener: T) {
        this.listeners.push(listener);
    }

    remove(removedListener: T) {
        this.listeners = this.listeners.filter((listener) => listener !== removedListener);
    }

    protected invoke(cb: (e: T) => void) {
        this.listeners.forEach(cb);
    }
}