import * as _ from "lodash";
import * as WebSocket from 'ws';
import {IHandlerExecutor} from "../infrastructure/IHandlerExecutor";
import {IClient} from "../infrastructure/transport/IClient";
import {ITransportStatusListener} from "../infrastructure/transport/ITransportStatusListener";
import {ILogWriter} from "../infrastructure/ILogWriter";
import * as uuid from 'uuid';
import {IServerMessageBuilder} from "../infrastructure/IServerMessageBuilder";
import {ServerMessageObject, ClientMessageObject} from "apollo-core/domain";
import {Observable, Subject} from "rxjs";
import {filter, mapTo} from "rxjs/operators";

class WebSocketClient implements IClient {
    constructor(private cid: string, private ws: WebSocket) {
    }

    clientId(): string {
        return this.cid;
    }

    kick(): void {
        this.ws.close();
    }

    sendMessage(message: ServerMessageObject): void {
        this.ws.send(JSON.stringify(message));
    }
}

export class WebSocketTransport {
    private clientMap = new Map<string, WebSocketClient>();

    constructor(private handlerMap: IHandlerExecutor, private statusListener: ITransportStatusListener, private logger: ILogWriter) {

    }

    listen() {
        const wss = new WebSocket.Server({ port: 8080 });


        wss.on("connection", (ws) => {
            const id = uuid.v4();
            const client = new WebSocketClient(id, ws);
            this.clientMap.set(client.clientId(), client);

            ws.on("message", async (message) => {
                if (!_.isString(message)) return;

                const packet: ClientMessageObject = JSON.parse(message);

                await this.handlerMap.findHandlerAndExecute(client, packet.pid, packet.pdata);
            });

            ws.on("close", (message) => {
                this.statusListener.onClientDisposed(client);
            });

            ws.on("error", (err) => {
                this.statusListener.onClientDisposed(client);
            });
        });

        this.logger.info("WebSocket Server listening on 8080")
    }

}