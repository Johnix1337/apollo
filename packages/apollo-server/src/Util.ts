export function notEmpty<T>(e: T | null): e is T {
    return !!e;
}