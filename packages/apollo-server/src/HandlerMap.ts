import {IHandlerExecutor} from "./infrastructure/IHandlerExecutor";
import {IClient} from "./infrastructure/transport/IClient";
import {IPacketHandler} from "./infrastructure/IPacketHandler";
import {IPacketHandlerRegister} from "./infrastructure/IPacketHandlerRegister";

export class HandlerMap implements IHandlerExecutor, IPacketHandlerRegister {
    private handlers: Map<string, IPacketHandler> = new Map();

    async findHandlerAndExecute(client: IClient, type: string, payload: any): Promise<void> {
        const handler = this.handlers.get(type);
        if (!handler) return;

        return handler.execute(client, payload);
    }

    addPacketHandler(type: string, handler: IPacketHandler): void {
        this.handlers.set(type, handler);
    }
}