import {IAlertManager} from "./infrastructure/alert/IAlertManager";
import {IMessageReceiver} from "./infrastructure/IMessageReceiver";
import {IAlert} from "./infrastructure/alert/IAlert";
import {IUserLike} from "./infrastructure/IUserLike";

export class AlertMessageBuilder {

}

export class AlertManager implements IAlertManager {
    sendAlert(client: IMessageReceiver, alert: IAlert): void {
    }

    sendGlobalAlert(alert: IAlert): void {
    }

    sendUserAlert(user: IUserLike, alert: IAlert): void {
    }

}