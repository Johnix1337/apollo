import {IRoomManager} from "./infrastructure/room/IRoomManager";
import {IRoomFacade} from "./infrastructure/room/IRoomFacade";
import {Subscription} from "rxjs";
import * as EasyStar from 'easystarjs';
import {IUserLike} from "./infrastructure/IUserLike";
import {IRoomEntity} from "./infrastructure/room/IRoomEntity";
import {IRoomPositionable} from "./infrastructure/room/IRoomPositionable";
import {IRoomUser} from "./infrastructure/room/IRoomUser";
import {ServerMessages} from "./message/ServerMessages";
import createUpdatedRoomPosition = ServerMessages.createUpdatedRoomPosition;
import createRoomUserInfoUpdate = ServerMessages.createUserJoinedRoom;
import createRoomDetails = ServerMessages.createRoomDetails;
import {RoomUserInfo, RoomUserPositionChange, RoomPosition, UserInfo, ServerMessageObject, RoomDetails, RoomInfo,
    RoomNavigatorInfo} from "apollo-core/domain";
import {IConnectedUser} from "./infrastructure/manageduser/IConnectedUser";
import {IMessageReceiver} from "./infrastructure/IMessageReceiver";

interface IRoomInteraction {
    sendRoomUserInfoUpdate(info: RoomUserInfo): void;
    sendUpdatedRoomPosition(positionUpdate: RoomUserPositionChange): void;

    registerEntity(entity: IRoomEntity): void;
    registerPositionable(positionable: IRoomPositionable): void;

    deleteEntity(entity: IRoomEntity): void;
    deletePositionable(positionable: IRoomPositionable): void;

    calculatePath(start: RoomPosition, end: RoomPosition): Promise<RoomPosition[]>;
    canWalkTo(position: RoomPosition): boolean;
}

/**
 * if (diffX == -1 && diffY == -1) return 7;
 if (diffX ==  0 && diffY == -1) return 0;
 if (diffX ==  1 && diffY == -1) return 1;

 if (diffX ==  1 && diffY == 0) return 2;

 if (diffX ==  1 && diffY == 1) return 3;
 if (diffX ==  0 && diffY == 1) return 4;
 if (diffX == -1 && diffY == 1) return 5;

 if (diffX == -1 && diffY == 0) return 6;


 const directionMapping = {
    "-1": { "-1": 7, "0": 0, "1": 1 },
    "0": { "" }
};

 */

class RoomUser implements IRoomEntity, IRoomUser, IUserLike, IMessageReceiver {
    private currentPosition: RoomPosition;
    private currentTarget: RoomPosition | null = null;
    private userInfoSubscription: Subscription;
    private searchingPath: boolean = false;
    private direction: number = 2;

    constructor(
        private initialPosition: RoomPosition,
        private managedUser: IConnectedUser,
        private roomManagerInteraction: IRoomInteraction,
        private roomUserMap: Map<string, RoomUser>
    ) {
        this.currentPosition = initialPosition;
        this.userInfoSubscription = this.managedUser
            .getUserInfoUpdates()
            .subscribe(this.onUserInfoUpdate.bind(this));
        this.roomManagerInteraction.registerPositionable(this);
        this.roomUserMap.set(managedUser.userId(), this);
        this.onUserInfoUpdate(managedUser.getUserInfo());
    }

    /**
     * @return RoomPosition Current position of user in the room
     */
    getCurrentPosition(): RoomPosition {
        return this.currentPosition;
    }

    /**
     * Callback when user info is updated
     * @param user New user info
     */
    private onUserInfoUpdate(user: UserInfo) {
        const roomUserInfo = this.getRoomUserInfo();
        if (!roomUserInfo) return;

        this.roomManagerInteraction.sendRoomUserInfoUpdate(roomUserInfo);
    }

    private handlePathFinding() {
        if (this.searchingPath || !this.currentTarget) return;

        this.searchingPath = true;
        this.roomManagerInteraction.calculatePath(this.currentPosition, this.currentTarget).then((path) => {
            this.searchingPath = false;


            if (path && path.length > 1) {
                console.log("Next move", path[1]);

                if (this.roomManagerInteraction.canWalkTo(path[1])) {
                    this.setPosition(path[1]);
                }
            }

            if (this.currentTarget && this.currentTarget.x == this.currentPosition.x && this.currentTarget.y == this.currentPosition.y) {
                console.log("Reached destination", this.currentTarget);
                this.currentTarget = null;
            }

            /*if (!path) {
                this.currentTarget = null;
            }*/
        });
    }

    next(): void {
        this.handlePathFinding();
    }

    sendMessage(message: ServerMessageObject): void {
        this.managedUser.sendMessage(message);
    }

    setTarget(position: RoomPosition): void {
        this.currentTarget = position;
        console.log("New target", position);
    }

    dispose(): void {
        console.info("Disposing Room User");
        this.userInfoSubscription.unsubscribe();
        this.roomManagerInteraction.deletePositionable(this);
        this.roomUserMap.delete(this.managedUser.userId());
    }

    setPosition(position: RoomPosition): void {
        const previous = this.currentPosition;
        this.currentPosition = position;

        this.direction = RoomUser.getDirection(previous, this.currentPosition);

        this.roomManagerInteraction.sendUpdatedRoomPosition({
            id: this.managedUser.userId(),
            position: this.currentPosition,
            direction: this.direction
        });
    }

    private static getDirection(previous: RoomPosition, next: RoomPosition): number {
        const diffX = next.x - previous.x;
        const diffY = next.y - previous.y;

        if (diffX == -1 && diffY == -1) return 7;
        if (diffX ==  0 && diffY == -1) return 0;
        if (diffX ==  1 && diffY == -1) return 1;

        if (diffX ==  1 && diffY == 0) return 2;

        if (diffX ==  1 && diffY == 1) return 3;
        if (diffX ==  0 && diffY == 1) return 4;
        if (diffX == -1 && diffY == 1) return 5;

        if (diffX == -1 && diffY == 0) return 6;

        return 2;
    }

    sendRoomDetails(details: RoomDetails) {
        console.log("Send room details");
        this.sendMessage(createRoomDetails(details));
    }

    getRoomUserInfo(): RoomUserInfo | null {

        return {
            id: this.managedUser.userId(),
            position: this.currentPosition,
            direction: this.direction,
            user_info: this.managedUser.getUserInfo(),
        };
    }

    isWalkable(): boolean {
        return false;
    }

    userId(): string {
        return this.managedUser.userId();
    }
}

function notEmpty<TValue>(value: TValue | null | undefined): value is TValue {
    return value !== null && value !== undefined;
}

class RoomManagerRoom implements IRoomFacade, IRoomInteraction {

    private idToRoomUser: Map<string, RoomUser> = new Map();
    private roomEntities: Set<IRoomEntity> = new Set();
    private roomPositionables: Set<IRoomPositionable> = new Set();

    private heightmap: number[][] = [

        [4,3,3,3,3,3,3],
        [2,1,1,1,1,1,1],
        [2,1,1,0,1,1,1],
        [2,1,1,0,0,1,1],
        [2,1,1,1,0,1,1],
        [2,1,1,1,1,1,1],
        [2,1,1,1,1,1,1],
        [2,1,1,0,0,1,1],
        [2,1,1,0,0,1,1],
        [2,1,1,1,1,1,1],
        [2,1,1,1,1,1,1],
        [2,1,1,1,1,1,1]
    ];

    constructor(private id: string) {
        setInterval(this.onTick.bind(this), 500);
    }

    onTick() {
        const arr = Array.from(this.roomEntities);
        arr.forEach((element) => element.next());
    }

    deleteEntity(entity: IRoomEntity): void {
        this.roomEntities.delete(entity);
    }

    registerEntity(entity: IRoomEntity): void {
        this.roomEntities.add(entity);
    }

    deletePositionable(positionable: IRoomPositionable): void {
        this.roomEntities.add(positionable);
        this.roomPositionables.delete(positionable);
    }

    registerPositionable(positionable: IRoomPositionable): void {
        this.roomEntities.add(positionable);
        this.roomPositionables.add(positionable);
    }

    private calculateHeightmap(): number[][] {
        var copyHeightmap = this.heightmap.map((arr) => {
            return arr.slice();
        });

        this.roomPositionables.forEach((positionable) => {
            if (positionable.isWalkable()) return;

            const { x,y } = positionable.getCurrentPosition();
            copyHeightmap[y][x] = 100;
        });

        return copyHeightmap;
    }

    private sendRoomDetails(roomUser: RoomUser) {
        const roomInfo: RoomInfo = {
            id: this.id,
            name: "Raum",
            description: "Das ist mein Raum.",
            heightmap: this.heightmap
        };

        const userInfos = Array.from(this.idToRoomUser.values()).map((v) => v.getRoomUserInfo()).filter(notEmpty);
        roomUser.sendRoomDetails({ room_info: roomInfo, user_infos: userInfos });
    }

    createRoomUser(client: IConnectedUser): IRoomUser {
        const roomUser = new RoomUser({ x: 2, y: 2 }, client, this, this.idToRoomUser);
        this.sendRoomDetails(roomUser);
        return roomUser;
    }


    calculatePath(start: RoomPosition, end: RoomPosition): Promise<RoomPosition[]> {

        const pathFinding = new EasyStar.js();
        pathFinding.setGrid(this.calculateHeightmap());
        pathFinding.setAcceptableTiles(1);
        pathFinding.enableDiagonals();

        return new Promise<RoomPosition[]>((resolve) => {
            pathFinding.findPath(start.x, start.y, end.x, end.y, (path) => resolve(path));
            pathFinding.calculate();
        });
    }

    canWalkTo(position: RoomPosition): boolean {
        return this.calculateHeightmap()[position.y][position.x] == 1;
    }

    sendToRoom(message: ServerMessageObject): void {
        Array.from(this.idToRoomUser.values()).forEach((ru) => ru.sendMessage(message));
    }

    sendRoomUserInfoUpdate(message: RoomUserInfo): void {
        this.sendToRoom(createRoomUserInfoUpdate(message));
    }

    sendUpdatedRoomPosition(message: RoomUserPositionChange): void {
        this.sendToRoom(createUpdatedRoomPosition(message));
    }

    getRoomNavigatorInfo(): RoomNavigatorInfo {
        return {
            id: this.id,
            name: "Test Raum",
            description: "Richtig cooler Raum",
            user_count: Array.from(this.idToRoomUser.values()).length
        };
    }


}

export class RoomManager implements IRoomManager {
    private roomMap: Map<string, RoomManagerRoom> = new Map();
    private roomByClientId: Map<string, IRoomUser> = new Map();

    constructor() {
    }

    async loadRoom(roomId: string): Promise<RoomManagerRoom> {

        console.log("Load room " + roomId);

        const presentRoom = this.roomMap.get(roomId);

        if (presentRoom) return presentRoom;

        const room = new RoomManagerRoom(roomId);
        this.roomMap.set(roomId, room);
        return room;
    }

    getRooms(): IRoomFacade[] {
        return Array.from(this.roomMap.values());
    }
}