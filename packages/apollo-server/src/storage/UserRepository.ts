import {IUserRepository} from "../infrastructure/repository/IUserRepository";

import {IUserLike} from "../infrastructure/IUserLike";
import {Connection, Repository} from 'typeorm';
import {UserInfo} from 'apollo-core/domain';
import {User} from "./entities/User";

export class UserRepository implements IUserRepository {
    private readonly repository: Repository<User>;

    constructor(private connection: Connection) {
        this.repository = connection.getRepository(User);
    }

    async findUserBySSO(sso: string): Promise<User | undefined> {
        const user = await this.repository.findOne({ where: { sso: sso } });
        if (!user) return;

        return user;
    }

    async findUserByUserLike(user: IUserLike): Promise<User | undefined> {
        return this.repository.findOne({ where: { id: user.userId() } });
    }

    async findUserInfo(userLike: IUserLike): Promise<UserInfo | undefined> {
        const user = await this.repository.findOne({ where: { id: userLike.userId() } });

        if (!user) return;

        return {
            name: user.name,
            motto: user.motto,
            id: user.id
        }
    }

    async changeMotto(user: IUserLike, newMotto: string): Promise<void> {
        await this.repository.update({ id: user.userId() }, { motto: newMotto });
    }

}