import {BaseEntity, Column, Entity, PrimaryGeneratedColumn} from "typeorm";
import {IUserLike} from "../../infrastructure/IUserLike";
import {UserInfo} from "apollo-core/domain";

@Entity()
export class User implements IUserLike, UserInfo {
    @PrimaryGeneratedColumn("uuid")
    id!: string;

    @Column()
    name!: string;

    @Column()
    sso!: string;

    @Column()
    motto!: string;

    userId(): string {
        return this.id;
    }
}