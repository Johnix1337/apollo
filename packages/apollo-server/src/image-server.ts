import * as express from 'express';
import * as request from 'request';

const app = express();

app.get("/image", (req, response: express.Response) => {
    request(
        `https://www.habbo.com/habbo-imaging/avatarimage?hb=image&user=Pulx&headonly=0&direction=${req.query.direction}&head_direction=${req.query.head_direction}&action=&gesture=spk&size=m`, {
            headers: {
                "User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36"
            }
        }).on('response', function(res) {
            res.headers["Access-Control-Allow-Origin"] = "*";
        }).pipe(response);
});

app.listen(9001);