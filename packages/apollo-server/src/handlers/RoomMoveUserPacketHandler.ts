import {IPacketHandler} from "../infrastructure/IPacketHandler";
import {IClient} from "../infrastructure/transport/IClient";
import {IRoomManager} from "../infrastructure/room/IRoomManager";
import {IGameDependencies} from "../infrastructure/IGameDependencies";
import {IConnectedUserFactory} from "../infrastructure/manageduser/IConnectedUserFactory";

export class RoomMoveUserPacketHandler implements IPacketHandler {
    private managedUserFactory: IConnectedUserFactory;
    private roomManager: IRoomManager;

    constructor({connectedUsers, roomManager}: IGameDependencies) {
        this.managedUserFactory = connectedUsers;
        this.roomManager = roomManager;
    }

    async execute(client: IClient, payload: any): Promise<void> {
        const user = this.managedUserFactory.getConnectedUserOfClient(client);
        if (!user) return;

        const roomUser = user.getRoomUser();

        if (!roomUser) return;
        roomUser.setTarget({ x: payload.x, y: payload.y });

    }
}