import {IPacketHandler} from "../infrastructure/IPacketHandler";
import {IClient} from "../infrastructure/transport/IClient";
import {IGameDependencies} from "../infrastructure/IGameDependencies";
import {IRoomManager} from "../infrastructure/room/IRoomManager";
import {IConnectedUserFactory} from "../infrastructure/manageduser/IConnectedUserFactory";
import {ServerMessages} from "../message/ServerMessages";

export class FetchNavigatorRoomsPacketHandler implements IPacketHandler {
    private managedUserFactory: IConnectedUserFactory;
    private roomManager: IRoomManager;

    constructor({ roomManager, connectedUsers }: IGameDependencies) {
        this.managedUserFactory = connectedUsers;
        this.roomManager = roomManager;
    }

    async execute(client: IClient, payload: any): Promise<void> {
        const user = this.managedUserFactory.getConnectedUserOfClient(client);
        if (!user) return;

        const roomInfos = this.roomManager.getRooms().map((room) => room.getRoomNavigatorInfo());

        user.sendMessage(ServerMessages.createFetchNavigatorRoomsSuccess(roomInfos))
    }
}