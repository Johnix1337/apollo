import {IPacketHandler} from "../infrastructure/IPacketHandler";
import {IClient} from "../infrastructure/transport/IClient";
import {IGameDependencies} from "../infrastructure/IGameDependencies";

export class ChatMessagePacketHandler implements IPacketHandler {
    constructor(dependencies: IGameDependencies) {

    }

    async execute(client: IClient, payload: any): Promise<void> {

    }
}