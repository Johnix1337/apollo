import {IPacketHandler} from "../infrastructure/IPacketHandler";
import {IClient} from "../infrastructure/transport/IClient";
import {IGameDependencies} from "../infrastructure/IGameDependencies";
import {IConnectedUserFactory} from "../infrastructure/manageduser/IConnectedUserFactory";

export class JoinRoomPacketHandler implements IPacketHandler {
    private managedUserFactory: IConnectedUserFactory;

    constructor({ connectedUsers }: IGameDependencies) {
        this.managedUserFactory = connectedUsers;
    }

    async execute(client: IClient, payload: any): Promise<void> {
        const user = this.managedUserFactory.getConnectedUserOfClient(client);
        if (!user) return;

        user.joinRoom("1");
    }
}