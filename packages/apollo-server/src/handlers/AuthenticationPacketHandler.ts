import {IPacketHandler} from "../infrastructure/IPacketHandler";
import {IClient} from "../infrastructure/transport/IClient";
import {IUserRepository} from "../infrastructure/repository/IUserRepository";
import {IGameDependencies} from "../infrastructure/IGameDependencies";
import {IConnectedUserFactory} from "../infrastructure/manageduser/IConnectedUserFactory";

export class AuthenticationPacketHandler implements IPacketHandler {
    private managedUserFactory: IConnectedUserFactory;
    private userRepository: IUserRepository;

    constructor({connectedUsers, userRepository}: IGameDependencies) {
        this.managedUserFactory = connectedUsers;
        this.userRepository = userRepository;
    }

    async execute(client: IClient, payload: any): Promise<void> {
        const user = await this.userRepository.findUserByUserLike({ userId: () => payload.id });
        console.log("Fetched user", user);

        if (!user) return;

        this.managedUserFactory.createConnectedUser(client, user);
    }
}