import {ServerMessageObject, RoomDetails, RoomUserPositionChange, RoomNavigatorInfo} from "apollo-core/domain";
import {RoomUserInfo} from "apollo-core/domain/RoomUserInfo";

export namespace ServerMessages {
    function createServerMessage(pid: string, pdata?: any): ServerMessageObject {
        return { pid: pid, pdata: pdata };
    }

    export function createAuthenticationSuccess(): ServerMessageObject {
        return createServerMessage("AUTHENTICATION_SUCCESS");
    }

    export function createOtherAccountLoggedIn(): ServerMessageObject {
        return createServerMessage("OTHER_ACCOUNT_LOGGED_IN");
    }

    export function createRoomDetails(details: RoomDetails): ServerMessageObject {
        return createServerMessage("ROOM_DETAILS", details);
    }

    export function createUpdatedRoomPosition(positionChange: RoomUserPositionChange): ServerMessageObject {
        return createServerMessage("UPDATED_ROOM_POSITION", positionChange);
    }

    export function createUserJoinedRoom(info: RoomUserInfo) {
        return createServerMessage("USER_JOINED_ROOM", info);
    }

    export function createFetchNavigatorRoomsSuccess(roomUsers: RoomNavigatorInfo[]) {
        return createServerMessage("FETCH_NAVIGATOR_ROOMS_SUCCESS", roomUsers);
    }
}