import {WebSocketTransport} from "./transport/WebSocketTransport";
import {HandlerMap} from "./HandlerMap";
import {TransportListenerCollection} from "./TransportListenerCollection";
import {ILogWriter} from "./infrastructure/ILogWriter";
import {AuthenticationPacketHandler} from "./handlers/AuthenticationPacketHandler";
import {ChatMessagePacketHandler} from "./handlers/ChatMessagePacketHandler";
import {JoinRoomPacketHandler} from "./handlers/JoinRoomPacketHandler";
import {RoomManager} from "./RoomManager";
import {RoomMoveUserPacketHandler} from "./handlers/RoomMoveUserPacketHandler";
import {IPlugin} from "./infrastructure/IPlugin";
import {IGameDependencies} from "./infrastructure/IGameDependencies";
import {AlertManager} from "./AlertManager";
import {FetchNavigatorRoomsPacketHandler} from "./handlers/FetchNavigatorRoomsPacketHandler";
import {ConnectedUserFactory} from "./ConnectedUserFactory";
import {UserRepository} from "./storage/UserRepository";
import {IUserRepository} from "./infrastructure/repository/IUserRepository";
import {User} from "./storage/entities/User";
import {UserInfo} from "apollo-core/domain";
import {IUserLike} from "./infrastructure/IUserLike";

class Log implements ILogWriter {
    info(...args: any[]): void {
        console.info(...args);
    }
}

class StubRepo implements IUserRepository {
    async findUserBySSO(sso: string): Promise<UserInfo | undefined> {
        return {
            id: "1",
            name: "Jan",
            motto: "Das ist mein Motto"
        };
    }

    async findUserByUserLike(user: IUserLike): Promise<UserInfo | undefined> {
        return {
            id: user.userId(),
            name: "Jan",
            motto: "Das ist mein Motto"
        };
    }

    async findUserInfo(user: IUserLike): Promise<UserInfo | undefined> {
        return this.findUserByUserLike(user);
    }

    changeMotto(user: IUserLike, newMotto: string): Promise<void> {
        return Promise.resolve();
    }

}

async function main() {
    const logger = new Log();
    const handlerMap = new HandlerMap();

    const transportListeners = new TransportListenerCollection();
    const alertManager = new AlertManager();

    const webSocketTransport = new WebSocketTransport(handlerMap, transportListeners, logger);
    const userRepository = new StubRepo();

    const roomManager = new RoomManager();
    await roomManager.loadRoom("1");

    const managedUserFactory = new ConnectedUserFactory(userRepository, roomManager, transportListeners);



    const dependencies: IGameDependencies = {
        connectedUsers: managedUserFactory,
        roomManager: roomManager,
        packetHandlerRegister: handlerMap,
        userRepository: userRepository
    };

    const plugins: IPlugin[] = [];

    handlerMap.addPacketHandler("AUTHENTICATE", new AuthenticationPacketHandler(dependencies));
    handlerMap.addPacketHandler("CHAT_MESSAGE", new ChatMessagePacketHandler(dependencies));
    handlerMap.addPacketHandler("JOIN_ROOM", new JoinRoomPacketHandler(dependencies));
    handlerMap.addPacketHandler("ROOM_MOVE_USER", new RoomMoveUserPacketHandler(dependencies));
    handlerMap.addPacketHandler("FETCH_NAVIGATOR_ROOMS", new FetchNavigatorRoomsPacketHandler(dependencies));

    logger.info("Loading plugins");
    plugins.forEach((plugin) => {
        logger.info(`Loading plugin: ${plugin.name}`);
        plugin.onCreate(dependencies);
    });

    webSocketTransport.listen();
}

main().catch(console.error);