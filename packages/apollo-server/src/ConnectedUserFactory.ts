import {IConnectedUserFactory} from "./infrastructure/manageduser/IConnectedUserFactory";
import {ConnectedUser} from "./ConnectedUser";
import {IUserLike} from "./infrastructure/IUserLike";
import {IUserRepository} from "./infrastructure/repository/IUserRepository";
import {IClient} from "./infrastructure/transport/IClient";
import {UserInfo} from "apollo-core/domain";
import {ServerMessages} from "./message/ServerMessages";
import createOtherAccountLoggedIn = ServerMessages.createOtherAccountLoggedIn;
import createAuthenticationSuccess = ServerMessages.createAuthenticationSuccess;
import {ITransportStatusListener} from "./infrastructure/transport/ITransportStatusListener";
import {TransportListenerCollection} from "./TransportListenerCollection";
import {IConnectedUser} from "./infrastructure/manageduser/IConnectedUser";
import {RoomManager} from "./RoomManager";

export class ConnectedUserFactory implements IConnectedUserFactory, ITransportStatusListener {
    private clientIdToUser: Map<string, ConnectedUser> = new Map();
    private userIdToClient: Map<string, IClient> = new Map();

    constructor(
        private readonly repository: IUserRepository,
        private readonly roomManager: RoomManager,
        private statusListeners: TransportListenerCollection
    ) {
        this.statusListeners.add(this);
    }

    createConnectedUser(client: IClient, userInfo: UserInfo): ConnectedUser {
        if (this.clientIdToUser.get(client.clientId())) {
            throw new Error("Client already authenticated");
        }

        //console.log("Authenticating", client.clientId(), "as", user.userId());

        const existingClient = this.userIdToClient.get(userInfo.id);

        const authUser = new ConnectedUser(client, this.roomManager, userInfo, this.repository);

        if (existingClient) {
            existingClient.sendMessage(createOtherAccountLoggedIn());
            existingClient.kick();
        }

        this.clientIdToUser.set(client.clientId(), authUser);
        this.userIdToClient.set(userInfo.id, client);

        client.sendMessage(createAuthenticationSuccess());

        return authUser;
    }

    onClientDisposed(client: IClient): void {
        const user = this.clientIdToUser.get(client.clientId());
        this.clientIdToUser.delete(client.clientId());

        if (user) {
            this.userIdToClient.delete(user.userId());
            user.dispose();
        }
    }

    getConnectedUserOfClient(client: IClient): ConnectedUser | undefined {
        return this.clientIdToUser.get(client.clientId());
    }
}