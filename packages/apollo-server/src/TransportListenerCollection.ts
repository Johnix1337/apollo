import {ITransportStatusListener} from "./infrastructure/transport/ITransportStatusListener";
import {IClient} from "./infrastructure/transport/IClient";
import {BaseListenerCollection} from "./BaseListenerCollection";

export class TransportListenerCollection extends BaseListenerCollection<ITransportStatusListener> implements ITransportStatusListener {
    onClientDisposed(client: IClient): void {
        this.invoke((listener) => listener.onClientDisposed(client));
    }
}