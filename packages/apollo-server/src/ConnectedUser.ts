import {IConnectedUser} from "./infrastructure/manageduser/IConnectedUser";
import {UserInfo, ServerMessageObject} from "apollo-core/domain";
import {BehaviorSubject, Observable, Subject} from "rxjs";
import {IUserRepository} from "./infrastructure/repository/IUserRepository";
import {IUserLike} from "./infrastructure/IUserLike";
import {IClient} from "./infrastructure/transport/IClient";
import {IRoomManager} from "./infrastructure/room/IRoomManager";
import {IRoomUser} from "./infrastructure/room/IRoomUser";
import {IDisposable} from "./infrastructure/IDisposable";
import {RoomManager} from "./RoomManager";
import {IRoomFacade} from "./infrastructure/room/IRoomFacade";

export class ConnectedUser implements IConnectedUser, IDisposable {
    private userInfo: UserInfo;
    private readonly subject: Subject<UserInfo> = new Subject();

    private roomUser?: IRoomUser;

    constructor(
        private client: IClient,
        private roomManager: RoomManager,
        initialUserInfo: UserInfo,
        private repository: IUserRepository
    ) {
        this.userInfo = initialUserInfo;
    }

    getUserInfo(): UserInfo {
        return this.userInfo;
    }

    getUserInfoUpdates(): Observable<UserInfo> {
        return this.subject;
    }

    userId(): string {
        return this.userInfo.id;
    }

    private updatedUserInfo(userInfo: Partial<UserInfo>) {
        this.userInfo = { ...this.userInfo, ...userInfo };
        this.subject.next(this.userInfo);
    }

    async changeMotto(motto: string) {
        await this.repository.changeMotto(this, motto);

        this.updatedUserInfo({ motto: motto });

        return this;
    }

    sendMessage(message: ServerMessageObject): void {
        this.client.sendMessage(message);
    }

    async joinRoom(roomId: string) {
        this.leaveCurrentRoom();

        const room = await this.roomManager.loadRoom(roomId);
        this.roomUser = room.createRoomUser(this);
    }

    leaveCurrentRoom() {
        if (!this.roomUser) return;
        this.roomUser.dispose();
        this.roomUser = undefined;
    }

    dispose(): void {
        console.log("Disposing ConnectedUser");
        this.leaveCurrentRoom();
    }

    getRoomUser(): IRoomUser | undefined {
        return this.roomUser;
    }

}