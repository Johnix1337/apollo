import {ServerMessageObject} from "apollo-core/domain";

export interface IMessageReceiver {
    sendMessage(message: ServerMessageObject): void;
}