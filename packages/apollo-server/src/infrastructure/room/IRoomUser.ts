import {IRoomPositionable} from "./IRoomPositionable";
import {RoomPosition} from "apollo-core/domain";
import {IDisposable} from "../IDisposable";

export interface IRoomUser extends IRoomPositionable, IDisposable {
    /**
     * Sets the target of user for path finding
     * @param position
     */
    setTarget(position: RoomPosition): void;

    /**
     * Set position of user in room
     * @param position
     */
    setPosition(position: RoomPosition): void;

}