/**
 * Interface for room entities
 */
export interface IRoomEntity {
    /**
     * Game loop method of the room. Called every 500ms
     */
    next(): void;
}