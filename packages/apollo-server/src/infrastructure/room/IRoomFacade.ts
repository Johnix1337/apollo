import {IUserLike} from "../IUserLike";
import {IRoomUser} from "./IRoomUser";
import {RoomNavigatorInfo} from "apollo-core/domain";


export interface IRoomFacade {
    /**
     * Creates a RoomUser. Transfers ownership to the caller
     * @param client
     */
    createRoomUser(client: IUserLike): IRoomUser;
    getRoomNavigatorInfo(): RoomNavigatorInfo;
}