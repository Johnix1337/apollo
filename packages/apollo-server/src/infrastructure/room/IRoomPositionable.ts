import {IRoomEntity} from "./IRoomEntity";
import {RoomPosition} from "apollo-core/domain";

/**
 * Interface for room entities which are positionable in the room
 */
export interface IRoomPositionable extends IRoomEntity {
    getCurrentPosition(): RoomPosition;

    isWalkable(): boolean;
}