import {IUserLike} from "../IUserLike";
import {IRoomFacade} from "./IRoomFacade";
import {IClient} from "../transport/IClient";
import {IRoomUser} from "./IRoomUser";

export interface IRoomManager {
    loadRoom(roomId: string): Promise<IRoomFacade>;
    getRooms(): IRoomFacade[];
}