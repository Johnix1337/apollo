import {IGameDependencies} from "./IGameDependencies";

export interface IPlugin {
    name: string;

    onCreate(dependencies: IGameDependencies): void;
}