import {ServerMessageObject} from "apollo-core/domain";

export interface IServerMessageBuilder {
    build(): ServerMessageObject;
}