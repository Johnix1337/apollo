import {IUserLike} from "../IUserLike";
import {ConnectedUser} from "../../ConnectedUser";
import {IConnectedUser} from "./IConnectedUser";
import {IClient} from "../transport/IClient";
import {UserInfo} from "../../../../apollo-core/domain";

/**
 * ConnectedUserFactory builds ManagedUsers.
 */
export interface IConnectedUserFactory {
    createConnectedUser(client: IClient, user: UserInfo): IConnectedUser;
    getConnectedUserOfClient(client: IClient): IConnectedUser | undefined;
}