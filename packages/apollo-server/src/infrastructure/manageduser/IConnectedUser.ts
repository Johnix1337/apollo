import {IUserLike} from "../IUserLike";
import {Observable} from "rxjs";
import {UserInfo} from "../../../../apollo-core/domain";
import {IMessageReceiver} from "../IMessageReceiver";
import {IRoomFacade} from "../room/IRoomFacade";
import {IRoomUser} from "../room/IRoomUser";

export interface IConnectedUser extends IUserLike, IMessageReceiver {
    getUserInfo(): UserInfo;
    getUserInfoUpdates(): Observable<UserInfo>;
    changeMotto(motto: string): Promise<this>;
    joinRoom(roomId: string): void;
    getRoomUser(): IRoomUser | undefined;
}