import {IClient} from "./transport/IClient";

export interface IPacketHandler {
    execute(client: IClient, payload: any): Promise<void>;
}