import {IClient} from "./IClient";

export interface ITransportStatusListener {
    onClientDisposed(client: IClient): void;
}