import {IMessageReceiver} from "../IMessageReceiver";
import {Observable} from "rxjs";
import {IDisposable} from "../IDisposable";

export interface IClient extends IMessageReceiver {
    clientId(): string;
    kick(): void;
}

export interface IClientDisposable extends IClient, IDisposable {

}