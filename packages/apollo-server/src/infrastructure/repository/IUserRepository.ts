import {IUserLike} from "../IUserLike";
import {UserInfo} from "apollo-core/domain";
import {User} from "../../storage/entities/User";

export interface IUserRepository {
    findUserBySSO(sso: string): Promise<UserInfo | undefined>;
    findUserByUserLike(user: IUserLike): Promise<UserInfo | undefined>;
    findUserInfo(user: IUserLike): Promise<UserInfo | undefined>;
    changeMotto(user: IUserLike, newMotto: string): Promise<void>;
}