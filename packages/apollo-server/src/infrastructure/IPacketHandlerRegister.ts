import {IPacketHandler} from "./IPacketHandler";

export interface IPacketHandlerRegister {
    addPacketHandler(type: string, handler: IPacketHandler): void;
}