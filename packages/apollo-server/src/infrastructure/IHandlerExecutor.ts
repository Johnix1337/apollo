import {IClient} from "./transport/IClient";

export interface IHandlerExecutor {
    findHandlerAndExecute(client: IClient, type: string, payload: any): Promise<void>;
}