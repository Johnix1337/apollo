import {IRoomManager} from "./room/IRoomManager";
import {IPacketHandlerRegister} from "./IPacketHandlerRegister";
import {IConnectedUserFactory} from "./manageduser/IConnectedUserFactory";
import {IUserRepository} from "./repository/IUserRepository";

export interface IGameDependencies {
    roomManager: IRoomManager;
    connectedUsers: IConnectedUserFactory,
    packetHandlerRegister: IPacketHandlerRegister,
    userRepository: IUserRepository
}