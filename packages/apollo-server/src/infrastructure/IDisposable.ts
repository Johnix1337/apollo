import {Observable} from "rxjs";

export interface IDisposable {
    dispose(): void;
}