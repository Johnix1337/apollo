import {IMessageReceiver} from "../IMessageReceiver";
import {IClient} from "../transport/IClient";
import {IAlert} from "./IAlert";
import {IUserLike} from "../IUserLike";



export interface IAlertManager {
    sendAlert(client: IMessageReceiver, alert: IAlert): void;
    sendUserAlert(user: IUserLike, alert: IAlert): void;
    sendGlobalAlert(alert: IAlert): void;
}